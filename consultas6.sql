﻿USE revista;


-- 1 Listar el título del artículo donde la descripción sea base de datos y el año sea 1990
SELECT * FROM tema t WHERE t.descripcion='base_de_datos';
SELECT a.titulo_ar FROM articulo a JOIN (SELECT * FROM tema t WHERE t.descripcion='base_de_datos') c1 ON a.codtema= c1.codtema WHERE a.año=1990;

-- 2 
SELECT  FROM tema t;

-- 3 listar el titulo de las revistas que unicamente tengan articulos que hablen de medicina
  SELECT r.referencia FROM revista r; 
  SELECT a.referencia FROM articulo a JOIN tema t ON a.codtema = t.codtema WHERE t.descripcion <> "medicina";

  SELECT c1.titulo_rev FROM (SELECT r.referencia,r.titulo_rev FROM revista r) c1 LEFT JOIN (SELECT a.referencia FROM articulo a 
  JOIN tema t ON a.codtema = t.codtema WHERE t.descripcion <> "medicina") c2 ON c1.referencia= c2.referencia WHERE c2.referencia IS NULL;
  
  
-- 4 listar el nombre de los autores cuyos articulos sean  del año 91 que tengan la descripcion sql y
--  en el año 92 que tengan la descripcion sql
  SELECT a.dni FROM articulo a JOIN tema t ON a.codtema = t.codtema WHERE a.año='1991' AND  t.descripcion='sql'; 
  
  
-- 5 Listar el título del artículo con el año 1993 y que sea de la universidad politecnica de madrid
  SELECT a1.nombre FROM articulo a JOIN autor a1 ON a.dni = a1.dni WHERE a.año='1993' AND a1.universidad='politecnica_de_madrid';
