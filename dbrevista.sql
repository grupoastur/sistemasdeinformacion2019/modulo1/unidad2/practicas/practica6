﻿DROP DATABASE IF EXISTS revista;
CREATE DATABASE IF NOT EXISTS revista;
USE revista;

CREATE TABLE autor (
dni int,
nombre varchar (30),
universidad varchar (40),
PRIMARY KEY (dni) 
);
CREATE TABLE tema (
codtema int,
descripcion varchar (30),
PRIMARY KEY (codtema)
);
CREATE TABLE revista(
referencia int,
titulo_rev varchar (20),
editorial varchar (20),
PRIMARY KEY (referencia)
);
CREATE TABLE articulo (
dni int,
codtema int,
referencia int,
titulo_ar varchar (20),
año varchar (20),
volumen varchar (20),
numero int,
paginas int,
PRIMARY KEY (dni, codtema, referencia),
CONSTRAINT artiautor FOREIGN KEY (dni) REFERENCES autor (dni),
CONSTRAINT artitema FOREIGN KEY (codtema) REFERENCES tema (codtema),
CONSTRAINT artirevis FOREIGN KEY (referencia) REFERENCES revista (referencia)
);

USE revista;

INSERT INTO autor (dni, nombre, universidad) VALUES
('54765432d','gilberto','menendez'),
('86504938j','cuasimodo','politecnica_de_madrid'),
('34356432k','celedonio','pontificia'),
('88700845u','ambrosio','diocesana'),
('47878866l','fulgencio','etrusca');
INSERT INTO tema (codtema, descripcion) VALUES
('1','medicina'),
('2','cientific'),
('3','sql'),
('4','pintura'),
('5','base_de_datos');
INSERT INTO revista (referencia, titulo_rev, editorial) VALUES
('5','el_plasma','bruguera'),
('6','la_ciencia','casca'),
('9','la_talla','tallarina'),
('7','dalis','pintoresca'),
('4','cosas','manualidades');
INSERT INTO articulo (dni, codtema, referencia, titulo_ar, año, volumen, numero, paginas) VALUES
('54765432d','1','5','flotacion','1991','grande','4','343'),
('86504938j','2','6','aliens','1993','pequeño','22','241'),
('34356432k','3','9','esculti','1994','mediano','12','180'),
('88700845u','4','7','pinta','1999','brutal','3','400'),
('47878866l','5','4','manitas','1990','mini','8','200');
